#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * The memmem() function finds the start of the first occurrence of the
 * substring 'needle' of length 'nlen' in the memory area 'haystack' of
 * length 'hlen'.
 *
 * The return value is a pointer to the beginning of the sub-string, or
 * NULL if the substring is not found.
 */
void *memmem(const void *haystack, size_t hlen, const void *needle, size_t nlen)
{
    int needle_first;
    const void *p = haystack;
    size_t plen = hlen;

    if (!nlen)
        return NULL;

    needle_first = *(unsigned char *)needle;

    while (plen >= nlen && (p = memchr(p, needle_first, plen - nlen + 1)))
    {
        if (!memcmp(p, needle, nlen))
            return (void *)p;

        p++;
        plen = hlen - (p - haystack);
    }

    return NULL;
}

 int main (int argc, char *argv[]) {
  FILE * pFile;
  long lSize;
  char * buffer;
  size_t result;
  int try_search = 1;
  char * filename = argv[1];
  char * string = argv[2];
  int chars_before = atoi(argv[3]);
  int chars_after = atoi(argv[4]);

  pFile = fopen ( filename , "rb" );
  if(pFile==NULL){
   fputs ("File error",stderr);
   exit (1);
  }

  fseek (pFile , 0 , SEEK_END);
  lSize = ftell (pFile);
  rewind (pFile);

  //printf("File is %d bytes big\n", lSize);

  buffer = (char*) malloc (sizeof(char)*lSize);
  if(buffer == NULL){
   fputs("Memory error",stderr);
   exit (2);
  }

  result = fread (buffer,1,lSize,pFile);
  if(result != lSize){
   fputs("Reading error",stderr);
   exit (3);
  }

  //Logic to check for hex/binary/dec
  unsigned int string_length = strlen(argv[2]);
  char needle[string_length];
  char *last_needle = NULL;
  memcpy(needle,string,string_length);
  
  //printf("\n\nSize of string = %d\n\n", string_length);
  
  char *sb = buffer;

  while (try_search) {
    char *p = memmem(sb, lSize, needle, string_length); 
    int diff = 0;
    if (!p) break;
    printf("\nFound:\n");
    last_needle = p;
    diff = p - buffer;
    for (int i=(0 - chars_before); i<chars_after; i++) {
    	printf("%c",buffer[diff+i]);
    }
    printf("\n");
    //try_search = 0;
    
    // Continue searching for needle
    lSize -= (p + string_length) - sb;
    sb = p + string_length;

  }

  printf("-----------------------------------------------------------------\n");

  fclose (pFile);
  free (buffer);
  return 0;
 }

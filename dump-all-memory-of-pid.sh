#!/bin/bash

arg1="$1"
arg2="$2"
arg3="$3"
arg4="$4"

memory_dump() {
	echo "memory_dump() - Lets create dump files"
	grep rw-p /proc/$arg1/maps \
	| sed -n 's/^\([0-9a-f]*\)-\([0-9a-f]*\) .*$/\1 \2/p' \
	| while read start stop; do \
	    gdb --batch --pid $arg1 -ex \
	        "dump memory $arg1-$start-$stop.dump 0x$start 0x$stop" 1>/dev/null; \
	done
	echo
}

process_files() {
	echo "process_file() - Search memory dump files"
	for f in *.dump ; do \
		if grep -q $arg2 ${f} ; then \
			echo "Found string "$arg2" in file $f - Processing this file"; \
			./dump_sniffer $f $arg2 $arg3 $arg4; \
		fi; \
	done
	echo
}

memory_dump
process_files
